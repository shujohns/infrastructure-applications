

variable "project_id" {
  description = "project id"
}

variable "region" {
  description = "region"
}

provider "google" {
  project = var.project_id
  region  = var.region
}

# VPC
resource "google_compute_network" "vpc" {
  name                    = "${var.project_id}-vpc"
  auto_create_subnetworks = "false"
}

# Subnet
resource "google_compute_subnetwork" "subnet" {
  name          = "${var.project_id}-subnet"
  region        = var.region
  network       = google_compute_network.vpc.name
  ip_cidr_range = "10.10.0.0/24"

  # Associate the internet gateway with the subnet
  secondary_ip_range {
    range_name    = "my-secondary-range"
    ip_cidr_range = "10.10.1.0/24"
  }
}


# # Internet gateway
# resource "google_compute_global_address" "igw_address" {
#   name          = "my-igw-address"
#   address_type  = "EXTERNAL"
# }

# resource "google_compute_route" "internet_gateway_route" {
#   name         = "internet-gateway-route"
#   network      = google_compute_network.vpc.name
#   dest_range = "0.0.0.0/0"
#   next_hop_gateway = "default-internet-gateway"
# }

# # Attach the public subnet to the internet gateway
# resource "google_compute_router_nat" "nat" {
#   name    = "${var.project_id}-router-nat"
#   router  = google_compute_route.internet_gateway_route.name
#   region  = var.region
#   source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
#   nat_ip_allocate_option = "AUTO_ONLY"
# }
