#!/bin/bash

cd ./infrastructure

terraform apply --auto-approve

gcloud container clusters get-credentials $(terraform output -raw kubernetes_cluster_name) --region $(terraform output -raw region)

EXTERNAL_IP=$(terraform output -raw kubernetes_cluster_host)

export EXTERNAL_IP

echo $EXTERNAL_IP

kubectl create namespace ingress-nginx

kubectl apply -k ../applications/

kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.7.0/aio/deploy/recommended.yaml

kubectl apply -f https://raw.githubusercontent.com/hashicorp/learn-terraform-provision-gke-cluster/main/kubernetes-dashboard-admin.rbac.yaml

kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep service-controller-token | awk '{print $1}')

    